# Swift Tic Tac Toe, with Minimax
Tic Tac Toe game written in Swift.

## Set Up and Build
Clone the repository to your computer. You will need Xcode to build the project (originally built using Xcode v. 6.2). Your version of Swift is determined by your version of Xcode.

Open up Xcode, navigate to ```File```, and open the project ```tttswift``` that you have downloaded.
In the top left hand corner of the Xcode interface you will see the project directory (this is just to the right of what looks like a play button and a stop button). Navigate up to TTT - then click the play button to build the project.

## Starting the Game
In the top left hand corner of Xcode, use the dropdown to navigate to ```TTT-CL```. Press the play button to launch the game within the Xcode console.

## Instructions on running the test suite
In Xcode, select the TTT directory using the dropdown to the right of the stop button on the upper left hand side of the editor. Press Command-U to run all tests.
