import Foundation
import TTT

var board = Board(size: 3)
var factory = GameFactory(board: board)


var game = Game(players: factory.allPlayers(), board: board, UI: factory.createCommandLineInterface(), rules: factory.createRules())

game.start()