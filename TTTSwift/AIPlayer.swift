import Foundation

public class AIPlayer: Player {
    var board: Board
    public var token: Token
    public var order: Int
    public var move: Int
    public let maxDepth = 6
    
    public init(board: Board, token: Token, order: Int) {
        self.board = board
        self.token = token
        self.order = order
        move = 0
    }
    
    public func bestMove() -> Int {
        if middleNotTaken(board) {
            return 5
        }
        applyMinMax(self.board, token: self.token, depth: 0)
        return move
    }
    
    public func applyMinMax(board: Board, token: Token, depth: Int) -> Int {
        var minValue = -1000
        var maxValue = 1000
        
        var possibleMoves = [Int]()
        var scores = [Int]()
        
        if depth == maxDepth || gameOver(board) {
            return getScores(board, depth: depth)
        }
        
        for space in board.openSpaces() {
            board.placeToken(space, token: token)
            var score = applyMinMax(board, token: board.getOpponentToken(token), depth: depth + 1)
            scores.append(score)
            possibleMoves.append(space)
            board.resetCell(space)
        
        if maxPlayer(token.description) {
            if score > minValue {
                minValue = score
            }
        } else {
            if score < maxValue {
                maxValue = score
                }
            }
        }
    
        if minPlayer(token.description) {
            return maxValue
        }
        
        var index = find(scores, maxElement(scores))
        move = possibleMoves[index!]
        return minValue
    }
    
    public func getScores(board: Board, depth: Int) -> Int {
        if aiWon(board) {
            return 10 - depth;
        } else if opponentWon(board) {
            return depth - 10;
        } else {
            return 0
        }
    }
    
    private func gameOver(board: Board) -> Bool {
        return rules(board).hasWinner() || rules(board).hasDraw()
    }
    
    private func aiWon(board: Board) -> Bool {
        return winnerIsAI(board)
    }
    
    private func opponentWon(board: Board) -> Bool {
        return !winnerIsAI(board) && winningStringIsNotEmpty(board)
    }
    
    private func winnerIsAI(board: Board) -> Bool {
        var rules = Rules(board: board)
        return rules.hasWinner() && rules.getWinningString().description == self.token.description
    }
    
    private func winningStringIsNotEmpty(board: Board) -> Bool {
        var rules = Rules(board: board)
        return rules.hasWinner() && rules.getWinningString().description != Token.Empty.description
    }
    
    private func maxPlayer(token: String) -> Bool {
        return token == self.token.description
    }
    
    private func minPlayer(token: String) -> Bool {
        return token != self.token.description
    }
    
    private func middleNotTaken(board: Board) -> Bool {
        return board.tokenAt(5) == Token.Empty
    }
    
    private func rules(board: Board) -> Rules {
        return Rules(board: board)
    }
}
