import Foundation

public class Board {
    public var size: Int
    public var cells = [[Token]]()
    
    public init(size: Int) {
        self.size = size
        cells = Array(count: size, repeatedValue: Array(count: size, repeatedValue: Token.Empty))
    }
    
    public func placeToken(space: Int, token: Token) {
        cells[indexOfSpace(space) % size][indexOfSpace(space) / size] = token
    }
    
    public func tokenAt(space: Int) -> Token {
        return cells[indexOfSpace(space) % size][indexOfSpace(space) / size]
    }
    
    public func cellCount() -> Int {
        return cells[0].count * cells[1].count
    }
    
    public func resetCell(space: Int) {
        cells[indexOfSpace(space) % size][indexOfSpace(space) / size] = Token.Empty
    }
    
    public func spaceOpen(space: Int) -> Bool {
        return cells[indexOfSpace(space) % size][indexOfSpace(space) / size] == Token.Empty
    }
    
    public func allCells() -> [[Token]] {
        return cells
    }
    
    public func full() -> Bool {
        return openSpaces().isEmpty
    }
    
    public func invalidMove(choice: Int) -> Bool {
       return choiceNegativeOrZero(choice) || choiceTooLarge(choice) || !spaceOpen(choice)
    }
    
    public func openSpaces() -> [Int] {
        var openSpaces = [Int]()
        
        for i in 1...cellCount() {
            if spaceOpen(i) {
                openSpaces.append(i)
            }
        }
        sort(&openSpaces)
        return openSpaces
    }
    
    public func choiceTooLarge(choice: Int) -> Bool {
        return choice > cellCount() ? true : false
    }
    
    public func choiceNegativeOrZero(choice: Int) -> Bool {
        return choice <= 0 ? true : false
    }
    
    public func getOpponentToken(token: Token) -> Token {
        var tokensOnBoard = [Token]()
        var opponentToken = [Token]()
        
        for i in 1...cellCount() {
            tokensOnBoard.append(tokenAt(i))
        }
        
        opponentToken = tokensOnBoard.filter({ $0 != Token.Empty && $0 != token })
        
        return opponentToken.isEmpty ? Token.Empty : opponentToken[0]
    }
    
    private func indexOfSpace(space: Int) -> Int {
        return space - 1
    }
}