import Foundation

public class CommandLineInterface : Messaging {
    var output: OutputStreamType
    
    public init(output: OutputStreamType) {
        self.output = output
    }
    
    public func showWelcome(boardSize: Int) {
        output.write("Welcome to Tic Tac Toe! The first player to get \(boardSize) in a row wins!")
    }
    
    public func showGamePieceInfo() {
        output.write("Player 1 will get the X piece and Player 2 will get the O piece.\(newLine())")
    }
    
    public func printBoardLine(string: String) {
        output.write(string)
    }
    
    public func playerOneStart() {
        output.write("Player X will start - please input your choice. Please pick between numbers 1 and 9.")
    }
    
    public func showBoard(board: Board) {
        var boardSquareRoot = board.size;
        var boardLength = board.cellCount()
        
        var boardToDisplay = boardRows(boardSquareRoot)
        
        for i in 1...boardLength  {
            boardToDisplay += sideOfCell()
            boardToDisplay += tokenAt(board, space: i)
            boardToDisplay += " "
            
            if (i - 1) % boardSquareRoot == boardSquareRoot - 1 {
                boardToDisplay += "|" + boardRows(boardSquareRoot)
            }
        }
        printBoardLine(boardToDisplay)
    }
    
    private func boardRows(squareRoot: Int) -> String {
        var rowsToDisplay = newLine()
        
        for i in 0...squareRoot {
            rowsToDisplay += "---"
        }
        
        rowsToDisplay += newLine()
        return rowsToDisplay;
    }
    
    public func showError(choice: Int) {
        output.write("Oops, \(choice) is not valid - try again!")
    }
    
    public func showSpaceTakenError() {
        output.write("Space is already taken - try again!")
    }
    
    public func showStringError(choice: String) {
        output.write("Please input a number.\(newLine())")
    }
    
    public func printChoice(token: String, space: Int) {
        output.write("Player \(token) chose space \(space).")
    }
    
    public func printCatsGame() {
        output.write("Cat's game!")
    }
    
    public func printGameStatus(piece: String) {
        output.write("Player \(piece) wins!")
    }
    
    private func sideOfCell() -> String {
         return ("| ")
    }
    
    private func tokenAt(board: Board, space: Int) -> String {
        return board.tokenAt(space).description
    }
    
    private func newLine() -> String {
        return "\n"
    }
}
