import Foundation

public class Game {
    public var board: Board
    public var players: [Player]
    var UI: Messaging
    var rules: Rules
    public var currentPlayer: Player
    
    public init(players: [Player], board: Board, UI: Messaging, rules: Rules) {
        self.board = board
        self.UI = UI
        self.rules = rules
        self.players = players
        currentPlayer = players[0]
    }
    
    public func start() {
        printIntro()
        play()
        printGameStatus(rules.winningPiece)
        UI.showBoard(board)
    }
    
    public func printIntro() {
        UI.showWelcome(board.size)
        UI.showGamePieceInfo()
        UI.playerOneStart()
    }
    
    public func play() -> Bool {
        while spacesLeft() {
            askForMove(currentPlayer)
            if rules.hasWinner() || fullBoard() {
                return false
            }
            switchPlayers()
        }
        return true
    }
    
    public func printGameStatus(piece: Token) {
        piece == Token.Empty ? UI.printCatsGame() : UI.printGameStatus(piece.description)
    }
    
    public func askForMove(currentPlayer: Player) {
        UI.showBoard(board)
        evaluateChoice(currentPlayer, choice: currentPlayer.bestMove())
    }
    
    public func evaluateChoice(player: Player, choice: Int) {
        if board.invalidMove(choice) {
            redoTurn(choice)
        } else {
            placeOnBoard(choice, player: player)
        }
    }

    public func setCurrentPlayer(player: Player) {
        currentPlayer = player;
    }
    
    public func switchPlayers() {
        setCurrentPlayer(findOtherPlayer())
    }
    
    private func findOtherPlayer() -> Player {
        var player = players.filter() { $0.token != self.currentPlayer.token }
        return player[0]
    }
    
    private func redoTurn(choice: Int) {
        showErrorMessage(choice)
        askForMove(currentPlayer)
    }
    
    private func placeOnBoard(choice: Int, player: Player) {
        board.placeToken(choice, token: player.token);
        UI.printChoice(player.token.description, space: choice);
    }
    
    private func spacesLeft() -> Bool {
        return !board.full()
    }
    
    private func fullBoard() -> Bool {
        return board.full()
    }
    
    private func showErrorMessage(choice: Int) {
        board.choiceTooLarge(choice) || board.choiceNegativeOrZero(choice) ? UI.showError(choice) : UI.showSpaceTakenError()
    }
}
