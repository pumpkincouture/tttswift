import Foundation

public class GameFactory {
    public var board: Board
    
    public init(board: Board) {
        self.board = board
    }
    
    public func allPlayers() -> [Player] {
        return [createHumanPlayer(), createComputerPlayer()]
    }
    
    public func createCommandLineInterface() -> Messaging {
        return CommandLineInterface(output: StderrOutputStream())
    }
    
    public func createRules() -> Rules {
        return Rules(board: board)
    }
    
    private func createHumanPlayer() -> Player {
        return HumanPlayer(token: Token.X, order: 1, inputManager: createInputManager(), keyCapture: createKeyInput())
    }
    
    private func createComputerPlayer() -> Player {
        return AIPlayer(board: board, token: Token.O, order: 2)
    }
    
    private func createInputManager() -> Validating {
        return InputManager(messages: createCommandLineInterface())
    }
    
    private func createKeyInput() -> KeyCapturing {
        return KeyInput()
    }
}