import Foundation

public class HumanPlayer: Player {
    public var token: Token
    public var order: Int
    var inputManager: Validating
    var keyCapture: KeyCapturing
    public var convertedChoice: Int
    
    public init(token: Token, order: Int, inputManager: Validating, keyCapture: KeyCapturing) {
        self.token = token
        self.order = order
        self.inputManager = inputManager
        self.keyCapture = keyCapture
        convertedChoice = 0
    }
    
    public func bestMove() -> Int {
        checkIfMoveValid(keyCapture.captureInput())
        return convertedChoice
    }
    
    public func checkIfMoveValid(choice: String) {
        if !inputManager.isValidChoice(choice) {
            promptUntilMoveValid()
        }
        else {
           convertedChoice = convertToInteger(choice)
        }
    }
    
    public func promptUntilMoveValid() {
        var choice = keyCapture.captureInput()
        checkIfMoveValid(choice)
    }
    
    public func convertToInteger(choice: String) -> Int {
        var trimmed = trimNewLine(choice)
        return trimmed.toInt()!
    }
    
    private func trimNewLine(choice: String) -> String {
        return choice.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
    }
}
