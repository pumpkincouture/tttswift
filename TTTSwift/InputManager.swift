import Foundation

public struct InputManager : Validating {
    var messages: Messaging
    
    public init(messages: Messaging) {
        self.messages = messages
    }
    
    public func isValidChoice(input: String) -> Bool {
        var trimmed = trimNewLine(input)
        return choiceIsInteger(trimmed)
    }
    
    private func choiceIsInteger(input: String) -> Bool {
        var choice = input.toInt()
        
        if let choice = choice {
            return true
        } else {
            displayError(input)
            return false
        }
    }
    
    public func displayError(input: String) {
        messages.showStringError(input)
    }
    
    private func trimNewLine(choice: String) -> String {
        return choice.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
    }
}
