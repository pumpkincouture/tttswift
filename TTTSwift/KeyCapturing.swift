import Foundation

public protocol KeyCapturing {
    
     mutating func captureInput() -> String
}
