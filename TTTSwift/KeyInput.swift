import Foundation

public struct KeyInput : KeyCapturing {
    
    public func captureInput() -> String {
        var keyboard = NSFileHandle.fileHandleWithStandardInput()
        var inputData = keyboard.availableData
        return NSString(data: inputData, encoding:NSUTF8StringEncoding) as String
    }
}
