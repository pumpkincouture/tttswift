import Foundation

public protocol Messaging {
    
    func showWelcome(boardSize: Int)
    
    func showGamePieceInfo()
    
    func playerOneStart()
    
    func printBoardLine(string: String)
    
    func showBoard(board: Board)
    
    func showError(choice: Int)
    
    func printChoice(token: String, space: Int)
    
    func showStringError(choice: String)
    
    func showSpaceTakenError()
    
    func printCatsGame()
    
    func printGameStatus(piece: String)
}
