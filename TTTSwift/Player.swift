import Foundation

public protocol Player {
    var token: Token { get set }
    var order: Int { get set }
    
    func bestMove() -> Int
}
