import Foundation

public class Rules {
    public var rowValues =  [[Token]]()
    public var columnValues = [[Token]]()
    public var leftDiagValues = [Token]()
    public var rightDiagValues = [Token]()
    public var winningPiece = Token.Empty
    public var board: Board
    
    public init(board: Board) {
        self.board = board
    }
    
    public func hasWinner() -> Bool {
        checkGrid()
        getBoardValues()
        checkForWinner()
        return winnerOnBoard()
    }
    
    public func hasDraw() -> Bool {
        return board.full() && winningPiece == Token.Empty
    }
    
    public func getWinningString() -> Token {
        return winningPiece
    }
    
    private func winnerOnBoard() -> Bool {
        return winningPiece != Token.Empty
    }
    
    private func generateValues() -> [[Token]] {
        return Array(count: board.size, repeatedValue: Array(count: board.size, repeatedValue: Token.Empty))
    }
    
    private func generateOneDValues() -> [Token] {
        return Array(count: board.size, repeatedValue: Token.Empty)
    }
    
    private func getBoardValues() {
        var maxIndex = board.size - 1
        
        for column in 0..<board.size {
            for row in 0..<board.size {
                assignColumnAndRowValues(row, column: column, maxIndex: maxIndex)
            }
            assignDiagValues(column, maxIndex: maxIndex)
        }
    }
    
    private func assignColumnAndRowValues(row: Int, column: Int, maxIndex: Int) {
        rowValues[column][row] = board.allCells()[row][column]
        columnValues[column][row] = board.allCells()[column][maxIndex - row]
    }
    
    private func assignDiagValues(column: Int, maxIndex: Int) {
        leftDiagValues[column] = board.allCells()[column][column]
        rightDiagValues[column] = board.allCells()[column][maxIndex - column]
    }
    
    private func checkForWinner() {
        for line in 0..<rowValues.count {
            checkForWinningCombo(line, boardValues: rowValues)
            checkForWinningCombo(line, boardValues: columnValues)
        }
        checkForWinningCombo(leftDiagValues)
        checkForWinningCombo(rightDiagValues)
    }
    
    private func checkForWinningCombo(index: Int, boardValues: [[Token]]) {
        if isWinningCombo(boardValues[index]) {
            winningPiece = boardValues[index][0]
        }
    }
    
    private func checkForWinningCombo(boardValues: [Token]) {
        if isWinningCombo(boardValues) {
            winningPiece = boardValues[0]
        }
    }
    
    private func isWinningCombo(values: [Token]) -> Bool {
        var first = values[0]
        
        for elem in values {
            if elem != first || elem == Token.Empty {
                return false
            }
        }
        return true
    }
    
    private func checkGrid() {
        rowValues = generateValues()
        columnValues = generateValues()
        leftDiagValues = generateOneDValues()
        rightDiagValues = generateOneDValues()
    }
}
