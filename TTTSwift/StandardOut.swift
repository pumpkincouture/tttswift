import Foundation

public struct StderrOutputStream: OutputStreamType {
    public static let stream = stdout
    public func write(string: String) { println(string) }
    
    public init() {}
}
