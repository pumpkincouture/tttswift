import Foundation

public enum Token: Character, Printable {
    case X = "X"
    case O = "O"
    case Empty = "*"
    
    public var description: String {
        return "\(self.rawValue)"
    }
}
