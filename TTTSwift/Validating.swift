import Foundation

public protocol Validating {
    
    mutating func isValidChoice(input: String) -> Bool
}