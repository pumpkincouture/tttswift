import XCTest
import TTT

class AIPlayerTest: XCTestCase {
    
    let ai = AIPlayer(board: UsableBoards.ThreeByThreeBoard, token: .O, order: 2)
    
    func testAIPlayerHasToken() {
        XCTAssertEqual(ai.token.description, Token.O.description)
    }
    
    func testAIPlayerGoesSecondInGame() {
        XCTAssertEqual(ai.order, 2)
    }
    
    func testReturnsScoreOf10IfAIWins() {
        
        addMovesToBoard([
            .O,   .O,   .O,
            .X,    E,   .O,
            .O,   .X,   .O])
        
        
        XCTAssertEqual(ai.getScores(UsableBoards.ThreeByThreeBoard, depth: 0), 10)
    }
    
    func testReturnsScoreOfNegative10ForAILoss() {
        
        addMovesToBoard([
            .O,   .O,   .X,
            .X,   .X,   .O,
            .X,   .X,    E])
        
        
        XCTAssertEqual(ai.getScores(UsableBoards.ThreeByThreeBoard, depth: 0), -10)
    }
    
    func testReturnsScoreOfZeroForDraw() {
        
        addMovesToBoard([
            .O,   .O,   .X,
            .X,   .O,   .O,
            .O,   .X,   .X])
        
        
        XCTAssertEqual(ai.getScores(UsableBoards.ThreeByThreeBoard, depth: 0), 0)
    }
    
    func testThatAIMakesWinningMoveByTakingPosition5() {
        
        addMovesToBoard([
            .O,   .O,   .X,
            .X,    E,   .O,
            .O,   .X,   .O])
        
        XCTAssertEqual(ai.bestMove(), 5)
    }
    
    func testThatAIMakesWinningMoveIfTwoSpacesLeftAndChooses5() {
        
        addMovesToBoard([
            .O,   .X,   .O,
            .X,    E,   .O,
            .O,   .X,    E])
        
        XCTAssertEqual(ai.bestMove(), 5)
    }
    
    func testThatAIBlocksOpponentInLastMoveIfCannotWinOtherwise() {
        
        addMovesToBoard([
            .X,  .X,   E,
            .O,  .O,  .X,
            .X,  .O,   E]);
        
        XCTAssertEqual(ai.bestMove(), 3)
    }
    
    func testThatAIWinsRatherThanBlocksInTheNextMove() {
        
        addMovesToBoard([
            .X,  .O,  .O,
             E,   E,  .O,
            .X,  .O,  .X]);
        
        XCTAssertEqual(ai.bestMove(), 5)
    }
    
    func testThatAIGetsWinningMoveFourSpacesIntoGame() {
        
        addMovesToBoard([
            .X,  .O,  E,
            .X,  .O,  E,
             E,   E,  E])
        
        XCTAssertEqual(ai.bestMove(), 8)
    }
    
    func testThatAIReturnsBlockingMoveSixMovesIntoTheGame() {
        
        addMovesToBoard([
            .X, .O, .O,
             E, .O,  E,
            .X, .X,  E])
        
        XCTAssertEqual(ai.bestMove(), 4)
    }
    
    func testThatAIReturnsSpace6IfGoesSecondInTheGame() {
        
        addMovesToBoard([
            .O,  E,  E,
            .X, .X,  E,
             E,  E,  E])
        
        XCTAssertEqual(ai.bestMove(), 6)
        
    }
    
    func testThatAITakesMiddleSpaceAfterGoingSecond() {
        
        addMovesToBoard([
            .X,  E,  E,
             E,  E,  E,
             E,  E,  E])
        
         XCTAssertEqual(ai.bestMove(), 5)
    }
    
    func testThatAITakesSpace9ToBlock() {
        
        addMovesToBoard([
            .X, .O, .X,
            .O, .O, .X,
             E, .X,  E])
        
        XCTAssertEqual(ai.bestMove(), 9)
    }
}