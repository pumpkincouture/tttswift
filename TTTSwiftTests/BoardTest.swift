import XCTest
import TTT

class BoardTest: XCTestCase {
    
    let board = UsableBoards.ThreeByThreeBoard
    
    func testBoardHasSizeValue() {
        XCTAssertEqual(board.size, 3)
    }
    
    func testInitialBoardHasNineSpaces() {
        XCTAssertEqual(board.cellCount(), 9)
    }
    
    func testIfCellIsOpen() {
        
        addMovesToBoard([
            E,  E,  E,
            E,  E, .X,
            E, .O,  E])
        
        XCTAssertTrue(board.spaceOpen(4))
    }
    
    func testIfCellIsOccupied() {
        
        addMovesToBoard([
            E,  E,  E,
           .X,  E, .X,
            E, .O,  E])
        
        XCTAssertFalse(board.spaceOpen(4))
    }
    
    func testIfBoardIsFull() {
        
        addMovesToBoard([
            .O,  .O,  .X,
            .X,  .X,  .O,
            .O,  .X,  .O])
        
        XCTAssertTrue(board.full())
    }
    
    func testIfBoardIsEmpty() {
        
        XCTAssertFalse(board.full())
    }
    
    func testAllOpenCellsOnBoard() {
        
        addMovesToBoard([
            E,  E,  E,
            E,  E,  E,
            E,  E,  E])
        
        var openSpaces = [1, 2, 3, 4, 5, 6, 7, 8, 9]
        XCTAssertEqual(board.openSpaces(), openSpaces)
    }
    
    func testAllOpenSpacesLeftOnBoard() {
        
        addMovesToBoard([
            E,   E,  .X,
           .X,  .X,  .O,
            E,   E,   E])
        
        var openSpaces = [1, 2, 7, 8, 9]
        XCTAssertEqual(board.openSpaces(), openSpaces)
    }
    
    func testThatCellHasBeenReset() {
        
        addMovesToBoard([
            .O,  .O,  .X,
            .X,  .X,  .O,
            .O,  .X,  .O])
        
        board.resetCell(3)
        
        XCTAssertEqual(board.tokenAt(3), Token.Empty)
    }
    
    func testIfEmptyBoardReturnsEmptySpaceToken() {
        
        addMovesToBoard([
            E,  E,  E,
            E,  E,  E,
            E,  E,  E])
        
        XCTAssertEqual(board.getOpponentToken(Token.O), Token.Empty)
    }
    
    func testThatBoardGetsOpponentToken() {
        addMovesToBoard([
            .O,  .O,  .X,
            .X,   E,  .O,
            .O,  .X,  .O])
        
        XCTAssertEqual(board.getOpponentToken(Token.O), Token.X)
    }
    
    func testInvalidMoveWithLargeNumber() {
        XCTAssertTrue(board.invalidMove(100))
    }
    
    func testInvalidMoveWithNegativeNumber() {
        XCTAssertTrue(board.invalidMove(-100))
    }
    
    func testInvalidMoveWith0() {
        XCTAssertTrue(board.invalidMove(0))
    }
    
    func testValidMove() {
        board.resetCell(4)
        
        XCTAssertFalse(board.invalidMove(4))
    }
}
