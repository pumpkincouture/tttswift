import XCTest
import TTT

class CommandLineInterfaceTest: XCTestCase {

    var output = MockOutput()
    var ui: Messaging!
    var board: Board!
    
    override func setUp() {
        ui = CommandLineInterface(output: output)
    }

    func testWelcomeMessagePrinted() {
        ui.showWelcome(3)
        
        XCTAssertEqual(output.printedMessage(), "Welcome to Tic Tac Toe! The first player to get 3 in a row wins!")
    }
    
    func testGamePieceInfoPrinted() {
        ui.showGamePieceInfo()
        
        XCTAssertEqual(output.printedMessage(), "Player 1 will get the X piece and Player 2 will get the O piece.\n")
    }
    
    func testBoardIsDisplayed() {
        board = UsableBoards.ThreeByThreeBoard
        
        addMovesToBoard([
            .X, .O, E,
            .X, .X, E,
            .O, .O, E])
        
        ui.showBoard(board)
        
        XCTAssertEqual(output.printedMessage(),
            "\n" +
            "------------\n" +
            "| X | O | * |\n" +
            "------------\n" +
            "| X | X | * |\n" +
            "------------\n" +
            "| O | O | * |\n" +
            "------------\n")
    }
    
    func testPlayerOneStartDisplayed() {
        ui.playerOneStart()
        
        XCTAssertEqual(output.printedMessage(), "Player X will start - please input your choice. Please pick between numbers 1 and 9.")
    }
    
    func testErrorDisplayed() {
        ui.showError(1)
        
        XCTAssertEqual(output.printedMessage(), "Oops, 1 is not valid - try again!")
    }
    
    func testStringErrorDisplayed() {
        ui.showStringError("wtfbbq")
        
        XCTAssertEqual(output.printedMessage(), "Please input a number.\n")
    }
    
    func testSpaceTakenErrorDisplayed() {
        ui.showSpaceTakenError()
        
        XCTAssertEqual(output.printedMessage(), "Space is already taken - try again!")
    }

    func testChoiceMessageDisplayed() {
        ui.printChoice(Token.X.description, space: 2)
        
        XCTAssertEqual(output.printedMessage(), "Player X chose space 2.")
    }
    
    func testCatsGameMessageDisplayed() {
        ui.printCatsGame()
        
        XCTAssertEqual(output.printedMessage(), "Cat's game!")
    }
    
    func testGameStatusMessageDisplayed() {
        ui.printGameStatus("O")
        
        XCTAssertEqual(output.printedMessage(), "Player O wins!")
    }

}
