import XCTest
import TTT

class GameFactoryTest: XCTestCase {
    
    var gameFactory: GameFactory!
    var board: Board!

    override func setUp() {
        board = Board(size: 3)
        gameFactory = GameFactory(board: board)
    }
    
    func testThatGameFactoryHasBoardWithSize3() {
        
        XCTAssertEqual(gameFactory.board.size, board.size)
    }
    
    func testThatHumanPlayerCreatedWithTokenXAndGoesFirst() {
        
        XCTAssertEqual(gameFactory.allPlayers()[0].token, Token.X)
        XCTAssertEqual(gameFactory.allPlayers()[0].order, 1)
    }
    
    func testThatComputerPlayerCreatedWithTokenOAndGoesSecond() {
        
        XCTAssertEqual(gameFactory.allPlayers()[1].token, Token.O)
        XCTAssertEqual(gameFactory.allPlayers()[1].order, 2)
    }
    
    func testThatRulesHasBeenCreatedWithTheBoard() {
        XCTAssertEqual(gameFactory.createRules().board.size, board.size)
    }
}
