import XCTest
import TTT

class GameTest: XCTestCase {
    
    var messages = MockMessaging(output: MockOutput())
    var game: Game!
    var humanPlayer: HumanPlayer!
    var compPlayer: AIPlayer!
    var board: Board!
    var rules: Rules!
    var mockHuman = MockMove(moves: [])
    var mockInputManager = MockInputManager()
    var keyCapture = MockKeyInput(moves: ["2", "5"])
    
    override func setUp() {
        humanPlayer = HumanPlayer(token: Token.X, order: 1, inputManager: mockInputManager, keyCapture: keyCapture)
        compPlayer = AIPlayer(board: UsableBoards.ThreeByThreeBoard, token: Token.O, order: 2)
        board = UsableBoards.ThreeByThreeBoard
        rules = Rules(board: board)
        game = Game(players: [humanPlayer, compPlayer], board: board, UI: messages, rules: rules)
    }

    func testThatGameHasTwoPlayers() {
        XCTAssertEqual(game.players.count, 2)
    }
    
    func testThatGameHasBoard() {
        XCTAssertEqual(game.board.size, UsableBoards.ThreeByThreeBoard.size)
    }
    
    func testThatIntroMessagesAreDisplayed() {
        game.printIntro()
        
        XCTAssertTrue(messages.welcomeMessageDisplayed())
        XCTAssertTrue(messages.gamePieceMessageDisplayed())
        XCTAssertTrue(messages.playerOneStartMessageDisplayed())
    }
    
    func testThatChosenMoveMessageCalled() {
        mockHuman = MockMove(moves: [3])
        
        game.evaluateChoice(humanPlayer, choice: mockHuman.move())
    
        XCTAssertTrue(messages.chosenMoveMessageDisplayed())
    }
    
    func testThatPlayerTurnsAreSwitched() {
        game.setCurrentPlayer(humanPlayer)
        
        game.switchPlayers()
        
        XCTAssertEqual(game.currentPlayer.token, compPlayer.token)
    }
    
    func testEndingGameStatementIfPieceIsEmpty() {
        game.printGameStatus(Token.Empty)
        
        XCTAssertTrue(messages.catsGameMessageDisplayed())
    }
    
    func testEndingGameStatementIfPieceIsNotEmpty() {
        game.printGameStatus(Token.X)
        
        XCTAssertTrue(messages.winnerMessageDisplayed())
    }
}

