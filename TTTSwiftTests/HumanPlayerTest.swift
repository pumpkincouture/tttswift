import XCTest
import TTT

class HumanPlayerTest: XCTestCase {
    
    var inputValidator = MockInputManager()
    var messages = MockMessaging(output: UsableStreams.output)
    var humanPlayer: HumanPlayer!
    var keyCapture = MockKeyInput(moves: ["2"])
 
    override func setUp() {
        humanPlayer = HumanPlayer(token: Token.X, order: 1, inputManager: inputValidator, keyCapture: keyCapture)
    }
    
    func testHumanPlayerHasToken() {
        XCTAssertEqual(humanPlayer.token, Token.X)
    }
    
    func testHumanPlayerGoesFirstInGame() {
        XCTAssertEqual(humanPlayer.order, 1)
    }
    
    func testThatPlayerProvidesCorrectInput() {
        humanPlayer.checkIfMoveValid(keyCapture.captureInput())
        
        XCTAssertEqual(humanPlayer.convertedChoice, 2)
    }
}
