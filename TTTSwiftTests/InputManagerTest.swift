import XCTest
import TTT

class InputManagerTest: XCTestCase {
    
    var messages: MockMessaging!
    var inputManager: InputManager!

    override func setUp() {
        messages = MockMessaging(output: UsableStreams.output)
    }
    
    func testThatInputIsNotAnInteger() {
        inputManager = InputManager(messages: messages)
        
        XCTAssertFalse(inputManager.isValidChoice("wtf"))
        XCTAssertTrue(messages.stringErrorCalled)
    }
    
    func testThatInputIsAnInteger() {
        inputManager = InputManager(messages: messages)
        
        XCTAssertTrue(inputManager.isValidChoice("1"))
        XCTAssertFalse(messages.stringErrorCalled)
    }
}
