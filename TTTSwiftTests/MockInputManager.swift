import Foundation
import TTT

public class MockInputManager : Validating {
    
     public func isValidChoice(input: String) -> Bool {
        return choiceIsInteger(input)
    }
    
    private func choiceIsInteger(input: String) -> Bool {
        var choice = input.toInt()
        
        if let choice = choice {
            return true
        } else {
            return false
        }
    }
}