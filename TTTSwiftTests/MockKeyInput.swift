import Foundation
import TTT

public struct MockKeyInput : KeyCapturing {
    var moves: [String]
    
    mutating public func captureInput() -> String {
        return firstMove()
    }
    
    mutating func addMove(move: String) {
        moves.append(move)
    }
    
    mutating func firstMove() -> String {
        var move = moves.first!
        moves.removeAtIndex(0)
        return move
    }
}