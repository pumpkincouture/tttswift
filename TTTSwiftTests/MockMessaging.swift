import Foundation
import TTT

public class MockMessaging : CommandLineInterface, Messaging {
    var welcomeMessageCalled = false
    var gamePieceMessageCalled = false
    var boardDisplayCalled = false
    var errorCalled = false
    var choiceCalled = false
    var stringErrorCalled = false
    var catsGameCalled = false
    var winnerCalled = false
    var playerOneCalled = false
    

    override init(output: OutputStreamType) {
       super.init(output: output)
    }
    
    public override func showWelcome(boardSize: Int) {
        welcomeMessageCalled = true
    }
    
    override public func showGamePieceInfo() {
        gamePieceMessageCalled = true
    }
    
    override public func showBoard(board: Board) {
        boardDisplayCalled = true
    }
    
    override public func showError(choice: Int) {
        errorCalled = true
    }
    
    override public func showStringError(choice: String) {
        stringErrorCalled = true
    }
    
    override public func playerOneStart() {
        playerOneCalled = true
    }
    
    override public func printChoice(token: String, space: Int) {
        choiceCalled = true
    }
    
    override public func printCatsGame() {
        catsGameCalled = true
    }
    
    override public func printGameStatus(piece: String) {
        winnerCalled = true
    }
    
    public func welcomeMessageDisplayed() -> Bool {
        return welcomeMessageCalled
    }
    
    public func gamePieceMessageDisplayed() -> Bool {
        return gamePieceMessageCalled
    }
    
    public func playerOneStartMessageDisplayed() -> Bool {
        return playerOneCalled
    }
    
    public func boardDisplayed() -> Bool {
        return boardDisplayCalled
    }
    
    public func errorDisplayed() -> Bool {
        return errorCalled
    }
    
    public func stringErrorDisplayed() -> Bool {
        return stringErrorCalled
    }
    
    public func chosenMoveMessageDisplayed() -> Bool {
        return choiceCalled
    }
    
    public func catsGameMessageDisplayed() -> Bool {
        return catsGameCalled
    }
    
    public func winnerMessageDisplayed() -> Bool {
        return winnerCalled
    }
}
