import Foundation

public struct MockMove {
    var moves: [Int]
    
    mutating func addMove(move: Int) {
        moves.append(move)
    }
    
    mutating func move() -> Int {
        var move = moves.first!
        moves.removeAtIndex(0)
        return move
    }
}
