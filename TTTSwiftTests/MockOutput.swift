import Foundation

public class MockOutput : OutputString, OutputStreamType {
    var printedToScreen = ""
    
     public func write(string: String) {
        print(string, &printedToScreen)
    }
    
    public func printedMessage() -> String {
        return printedToScreen
    }
}
