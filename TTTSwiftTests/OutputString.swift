import Foundation

public protocol OutputString {
    
    func printedMessage() -> String
}