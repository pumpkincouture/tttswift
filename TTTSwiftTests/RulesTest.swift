import XCTest
import TTT

class RulesTest: XCTestCase {
    
    let rules = Rules(board: UsableBoards.ThreeByThreeBoard)
    
    func testIfThereIsAWinnerOnTheBoard() {
        XCTAssertFalse(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.Empty)
    }
    
    func testIfXIsWinnerInRowOne() {
        
        addMovesToBoard([
            .X,  .X,  .X,
            .O,   E,  .O,
            .X,   E,  .X]);
        
        XCTAssertTrue(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.X)
    }
    
    func testIfThereINoWinnerInRowOne() {
        
        addMovesToBoard([
            .X,  .O,  .O,
            .O,   E,  .O,
            .X,   E,  .X]);
        
        XCTAssertFalse(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.Empty)
    }
    
    func testIfThereIsWinnerInRowTwo() {
        
        addMovesToBoard([
            .X,  .O,  .O,
            .X,  .X,  .X,
            .O,   E,   E]);
        
        XCTAssertTrue(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.X)
    }
    
    func testIfThereIsNotAWinnerInRowTwo() {
        
        addMovesToBoard([
            .X,   E,  .O,
            .X,  .O,  .X,
             E,   E,   E]);
        
        XCTAssertFalse(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.Empty)
    }
    
    func testIfThereIsWinnerInRowThree() {
        
        addMovesToBoard([
             E,   E,  .O,
            .X,   E,   E,
            .O,  .O,  .O]);
        
        XCTAssertTrue(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.O)
    }
    
    func testIfThereIsNoWinnerInRowThree() {
        
        addMovesToBoard([
             E,   E,  .O,
            .X,   E,   E,
            .O,  .X,  .O]);
        
        XCTAssertFalse(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.Empty)
    }
    
    func testIfThereIsWinnerInRowOneOnFourByFour() {
        
        let rules = Rules(board: UsableBoards.FourByFourBoard)
        
        addMovesToFourByFourBoard([
            .X, .X, .X, .X,
            .X,  E,  E,  E,
            .O,  E,  E,  E,
             E,  E,  E,  E]);
        
        XCTAssertTrue(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.X)
    }
    
    func testIfThereIsNoWinnerInRowOneOnFourByFour() {
        
        let rules = Rules(board: UsableBoards.FourByFourBoard)
        
        addMovesToFourByFourBoard([
            .X, .O, .X, .X,
            .O,  E,  E,  E,
            .X,  E,  E,  E,
            .X,  E,  E,  E]);
        
        XCTAssertFalse(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.Empty)
    }
    
    func testIfThereIsWinnerInColumnOne() {
        
        addMovesToBoard([
            .X,   E,  .O,
            .X,   E,   E,
            .X,  .X,  .O]);
        
        XCTAssertTrue(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.X)
    }
    
    func testIfThereIsNoWinnerInColumnOne() {
        
        addMovesToBoard([
            .X,   E,  .O,
            .X,   E,   E,
             E,  .X,  .O]);
        
        XCTAssertFalse(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.Empty)
    }
    
    func testIfThereIsWinnerInColumnTwo() {
        
        addMovesToBoard([
             E,  .X,  .O,
            .X,  .X,   E,
            .O,  .X,  .O]);
        
        XCTAssertTrue(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.X)
    }
    
    func testIfThereIsNoWinnerInColumnTwo() {
        
        addMovesToBoard([
             E,   E,  .O,
            .X,   E,   E,
            .O,  .X,  .O]);
        
        XCTAssertFalse(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.Empty)
    }
    
    func testIfThereIsWinnerInColumnThree() {
        
        addMovesToBoard([
             E,   E,  .O,
            .X,   E,  .O,
            .O,  .X,  .O]);
        
        XCTAssertTrue(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.O)
    }
    
    func testIfThereIsNoWinnerInColumnThree() {
        
        addMovesToBoard([
             E,   E,  .O,
            .X,   E,   E,
            .O,  .X,  .O]);
        
        XCTAssertFalse(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.Empty)
    }
    
    func testIfThereIsWinnerInColumnOneOnFourByFour() {
        
        let rules = Rules(board: UsableBoards.FourByFourBoard)
        
        addMovesToFourByFourBoard([
            .X, .O, .O, .X,
            .X,  E,  E,  E,
            .X,  E,  E,  E,
            .X,  E,  E,  E]);
        
        XCTAssertTrue(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.X)
    }
    
    func testIfThereIsNoWinnerInColumnOneOnFourByFour() {
        
        let rules = Rules(board: UsableBoards.FourByFourBoard)
        
        addMovesToFourByFourBoard([
            .X, .O, .X, .X,
            .O,  E,  E,  E,
            .X,  E,  E,  E,
            .X,  E,  E,  E]);
        
        XCTAssertFalse(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.Empty)
    }
    
    func testIfThereIsWinnerInLeftDiagonal() {
        
        addMovesToBoard([
            .X,   E,  .O,
            .O,  .X,   E,
             E,   E,  .X]);
        
        XCTAssertTrue(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.X)
    }
    
    func testIfThereIsNoWinnerInLeftDiagonal() {
        
        addMovesToBoard([
             E,   E,  .O,
            .X,  .X,   E,
            .O,  .X,  .O]);
        
        XCTAssertFalse(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.Empty)
    }
    
    func testIfThereIsWinnerInLeftDiagonalOnFourByFour() {
        
        let rules = Rules(board: UsableBoards.FourByFourBoard)
        
        addMovesToFourByFourBoard([
            .X, .O, .O, .O,
            .X, .X,  E,  E,
            .O,  E, .X,  E,
            .X,  E,  E, .X]);
        
        XCTAssertTrue(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.X)
    }
    
    func testIfThereIsNoWinnerInLeftDiagonalOnFourByFour() {
        
        let rules = Rules(board: UsableBoards.FourByFourBoard)
        
        addMovesToFourByFourBoard([
            .X, .X, .X,  E,
            .X,  E,  E,  E,
            .X,  E,  E,  E,
            .O,  E,  E,  E]);
        
        XCTAssertFalse(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.Empty)
    }
    
    func testIfThereIsWinnerInRightDiagonal() {
        
        addMovesToBoard([
             E,   E,  .X,
            .O,  .X,   E,
            .X,  .O,  .O]);
        
        XCTAssertTrue(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.X)
    }
    
    func testIfThereIsNoWinnerInRightDiagonal() {
        
        addMovesToBoard([
            E,   E,  .O,
           .X,   E,   E,
           .O,  .X,  .O]);
        
        XCTAssertFalse(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.Empty)
    }
    
    func testIfThereIsWinnerInRightDiagonalOnFourByFour() {
        
        let rules = Rules(board: UsableBoards.FourByFourBoard)
        
        addMovesToFourByFourBoard([
            .X, .O, .O, .X,
             E,  E, .X,  E,
            .O, .X,  E,  E,
            .X,  E,  E,  E]);
        
        XCTAssertTrue(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.X)
    }
    
    func testIfThereIsNoWinnerInRightDiagonalOnFourByFour() {
        
        let rules = Rules(board: UsableBoards.FourByFourBoard)
        
        addMovesToFourByFourBoard([
            .X, .O, .X, .X,
            .O,  E,  E,  E,
            .X,  E,  E,  E,
            .X,  E,  E,  E]);
        
        XCTAssertFalse(rules.hasWinner())
        XCTAssertEqual(rules.getWinningString(), Token.Empty)
    }
    
    func testThatThereIsADrawOnTheBoard() {
        
        addMovesToBoard([
            .O,  .O,  .X,
            .X,  .X,  .O,
            .O,  .X,  .O]);
        
        XCTAssertFalse(rules.hasWinner())
        XCTAssertTrue(rules.hasDraw())
    }
    
    
    func testThatThereIsNoDrawOnTheBoard() {
        
        addMovesToBoard([
            .O,  .O,  .X,
            .X,  .X,  .O,
            .X,  .O,  .O]);
        
        XCTAssertTrue(rules.hasWinner())
        XCTAssertFalse(rules.hasDraw())
    }
}
