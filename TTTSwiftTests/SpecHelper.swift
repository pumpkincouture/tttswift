import Foundation

let E = Token.Empty

struct BoardStruct {
    static var ThreeByThreeBoard = Board(size: 3)
    static var FourByFourBoard = Board(size: 4)
}

func addMovesToBoard(positions: [Token]) {
    placeTokensInCorrectPositions(BoardStruct.ThreeByThreeBoard, positions)
}

func addMovesToFourByFourBoard(positions: [Token]) {
    placeTokensInCorrectPositions(BoardStruct.FourByFourBoard, positions)
}

func placeTokensInCorrectPositions(board: Board, positions: [Token]) {
    var number = 0
    for i in positions {
        number += 1
        board.placeToken(number, token: positions[number - 1])
    }
}