import Foundation
import TTT

let E = Token.Empty

struct UsableBoards {
    static var ThreeByThreeBoard = Board(size: 3)
    static var FourByFourBoard = Board(size: 4)
}

struct UsableStreams {
    static var output = StderrOutputStream()
    static var input = NSFileHandle()
}

func addMovesToBoard(positions: [Token]) {
    placeTokensInCorrectPositions(UsableBoards.ThreeByThreeBoard, positions)
}

func addMovesToFourByFourBoard(positions: [Token]) {
    placeTokensInCorrectPositions(UsableBoards.FourByFourBoard, positions)
}

func placeTokensInCorrectPositions(board: Board, positions: [Token]) {
    var number = 0
    for i in positions {
        number += 1
        board.placeToken(number, token: positions[number - 1])
    }
}